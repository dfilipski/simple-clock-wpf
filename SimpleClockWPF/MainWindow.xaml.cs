﻿using SimpleClock.Library.ViewModels;
using System.Windows;
using System.Windows.Input;

namespace SimpleClockWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SimpleClockViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = new SimpleClockViewModel();
            DataContext = _viewModel;
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            DragMove();
        }

        private void MenuItemQuit_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItemAlwaysOnTop_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.IsTopMost = !_viewModel.IsTopMost;
        }

        private void MainWindow_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var factor = e.Delta > 0 ? 1.1d : 0.9d;
            Width *= factor;
            Height *= factor;
        }
    }
}
