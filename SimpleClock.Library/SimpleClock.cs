﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SimpleClock.Library
{
    public class SimpleClock : INotifyPropertyChanged
    {
        private DateTime _currentTime;
        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                if (value != _currentTime)
                {
                    _currentTime = value;
                    RaisePropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;


        public SimpleClock()
        {
            CurrentTime = DateTime.Now;
            Task.Run(async () =>
            {
                while (true)
                {
                    CurrentTime = DateTime.Now;
                    await Task.Delay(TimeSpan.FromMilliseconds(5));
                }
            });
        }

        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}