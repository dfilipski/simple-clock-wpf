﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System;

namespace SimpleClock.Library.ViewModels
{
    public class SimpleClockViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        private string _timeString = DateTime.Now.ToShortTimeString();
        public string TimeString
        {
            get => _timeString;
            set
            {
                if (_timeString != value)
                {
                    _timeString = value;
                    RaisePropertyChanged();
                }
            }
        }

        private bool _isTopMost = true;

        public bool IsTopMost
        {
            get => _isTopMost;
            set
            {
                if (_isTopMost != value)
                {
                    _isTopMost = value;
                    RaisePropertyChanged();
                }

            }
        }


        private SimpleClock _simpleClock;

        public SimpleClockViewModel()
        {
            _simpleClock = new SimpleClock();
            _simpleClock.PropertyChanged += _simpleClock_PropertyChanged;
        }

        private void _simpleClock_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "CurrentTime":
                    TimeString = _simpleClock.CurrentTime.ToShortTimeString();
                    break;
                default:
                    break;
            }
        }

        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
