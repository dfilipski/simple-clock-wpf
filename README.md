# SimpleClockWPF

A simple clock application that shows the time.
I plan to eventually make it frameless.

[Clock icons created by Freepik](https://www.flaticon.com/free-icons/clock)